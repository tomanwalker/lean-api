
// ## dep
var express = require('express');
var request = require('request');

var registry = require('./components/registry');
var utils = require('./lib/utils');
var log = require('./lib/logHelper');

var router = express.Router();

// ## export
var ns = { router: router};
module.exports = ns;

// ## funcs
ns.logDomain = function(req, res, next){
	
	var host = req.get('host').trim();
	var path = req.path;
	if( host.includes(':') ){
		host = host.split(':')[0];
	}
	
	log.info('base.use - start - domain = %s | method = %s | url = %s',
		host, req.method, req.originalUrl);
	
	res.locals.host = host;
	return next();
	
};
ns.findService = function(req, res, next){
	
	var host = res.locals.host;
	var path = req.path;
	
	var found = registry.find(host, path);
	log.debug('use - registry - found = %s', (found || {}).name );
	
	if( !found ){
		if( path === '/' ){
			return res.json({ok: true});
		}
		else {
			log.info('use - not found - domain = %s | path = %s', host, path);
			return res.status(404).json({ message: 'Not Found' });
		}
	}
	
	res.locals.target = found;
	return next();
};
ns.rewritePath = function(req, res, next){
	
	var found = res.locals.target;
	res.locals.url = req.originalUrl;
	
	if( !found.mount ){
		return next();
	}
	var targetPath = req.originalUrl.substring(found.mount.length);
	if( targetPath.length === 0 ){
		targetPath = '/';
	}
	
	res.locals.url = targetPath;
	return next();
};

ns.makeRequest = function(req, res, next){
	
	var found = res.locals.target;
	var targetPath = res.locals.url;
	var ind = 0;
	if( found.address.length > 1 ){ 
		 ind = utils.randomBetween(0, found.address.length - 1);
	}
	log.debug('makeRequest - start - name = %s | add = %j | ind = %s | url = %s', 
		found.name, found.address,  ind, targetPath);
	
	var timeRequestStart = new Date();
	var opts = {
		url: found.address[ind] + targetPath,
		headers: req.headers,
		timeout: 120000
	};
	if( ['POST', 'PUT', 'PATCH'].indexOf(req.method) && req.body ){
		opts.body = JSON.stringify(req.body);
	}
	
	request(opts, function(error, response, body, headers){
		log.debug('makeRequest - callback - err = %s', error);
		
		if(error){
			log.error('makeRequest - failed - error = %s', JSON.stringify(error));
			return res.status(500).send('Internal server error');
		}
		
		log.debug('makeRequest - done - status = %s | headers = %j', 
			response.statusCode, response.headers);
		if( body && body.length < 100 ){
			log.debug('makeRequest - done - body = %s', body);
		}
		
		var timeRequestEnd = new Date();
		var timeRequestDiff = (timeRequestEnd - timeRequestStart);
		log.info('makeRequest - Request time = %s sec', timeRequestDiff / 1000);
		
		res.status(response.statusCode);
		res.set(response.headers);
		res.set('x-powered-by', 'lean-api');
		var parsedBody = utils.parseTry(body);
		
		if( parsedBody ){
			return res.json( parsedBody );
		}
		else{
			return res.send( body );
		}
	});
};

//## routes
router.use('/', ns.logDomain, ns.findService, ns.rewritePath, ns.makeRequest);


