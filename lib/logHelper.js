
// ## external

// ## internal

// ## export
var ns = {};
module.exports = ns;

// ## funcs
var print = function(...arg){
	arg[0] = (new Date()).toISOString() + ' - app >> ' + arg[0];
	return console.log(...arg);
};

ns.debug = print;
ns.info= print;
ns.error= print;


