
var cloudRun = (
	process.env.CLOUD_RUN === 'true'
	|| process.env.NODE_ENV === 'production'
);

if( !cloudRun ){
	var secret = require('./secret.json');
	
	for( var s in secret ){
		if( typeof(process.env[s]) === 'undefined' ){
			process.env[s] = secret[s];
		}
	}
}

module.exports = {
	PORT: Number(process.env.PORT || 3000),
	APP_ROOT: __dirname,
	CLOUD_RUN: cloudRun,
	EMAIL: process.env.EMAIL
};


