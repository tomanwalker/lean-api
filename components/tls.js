
// ## dep
var fs = require('fs');
var Greenlock = require('greenlock');

var log = require('../lib/logHelper');
var config = require('../config');

// ## config
var dir = './cert/';

// ## export
var ns = {};
module.exports = ns;

// ## funcs
ns.certBot = async function(){
	
	var email = config.EMAIL;
	var devMode = !config.CLOUD_RUN;
	
	var greenlock = Greenlock.create({
		configDir: './greenlock.d/',
		packageRoot: config.APP_ROOT,
		packageAgent: 'lean-api',
		maintainerEmail: email,
		staging: devMode,
		notify: function(event, details) {
			if ('error' === event) {
				log.error(details);
			}
		}
	});
	
	var fullConfig = await greenlock.manager.defaults({
		agreeToTerms: true,
		subscriberEmail: email
	});
	
	ns.green = greenlock;
	return ns.green;
};
ns.createCert = async function(svc_list){
	
	var svc_tls = svc_list.filter(x => x.tls);
	log.debug('enc.createCert - start - svc = %s / %s', svc_tls.length, svc_list.length);
	
	if( svc_tls.length === 0 ){
		return false;
	}
	
	for(var i=0; i<svc_tls.length; i++){
		var x = svc_tls[i];
		var filePem = dir + x.name + '.pem';
		var fileKey = dir + x.name + '.key';
		
		var exists = fs.existsSync(filePem);
		log.debug('enc.createCert - loop.start - svc = %s | ex = %s', x.name, exists);
		if( exists ){
			return true;
		}
		
		await ns.certBot();
		
		var result = await ns.green.add({
			subject: x.domain,
			altnames: [x.domain]
		});
		
		log.debug('enc.createCert - loop.end - result = %j', Object.keys(result));
		
		var site = await ns.green.get({ servername: x.domain });
		if( !site ){
			log.debug(x.domain + ' was not found in any site config');
		}
		else {
			var privkey = site.pems.privkey;
			var fullchain = site.pems.cert + '\n' + site.pems.chain + '\n';
			//console.log(privkey);
			//console.log(fullchain);
			fs.writeSync(filePem, fullchain);
			fs.writeSync(fileKey, privkey);
			
		}
	
	}
	
};


