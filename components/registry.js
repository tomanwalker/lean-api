
// ## dep


// ## export
var ns = {};
module.exports = ns;

// ## config
/* Sample service-list
[
	{
		"name": "mz-operation",
		"mount": "/operation",
		"address": [ "http://0.0.0.0:49160" ]
	}
]
*/
ns.services = require('../service-list.json');

// ## funcs
ns.find = function(host, path){
	
	var domainMatch = ns.services.find(function(x){
		return x.domain === host;
	});
	if( domainMatch ){
		return domainMatch;
	}
	
	return ns.services.find(function(el){
		return path.startsWith(el.mount);
	});
};


