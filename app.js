
/* ###
 * Initial implementation - Alex Shkunov, 2019
 */

// ## dependencies
var express = require('express');

var registry = require('./components/registry');
var tls = require('./components/tls');

var log = require('./lib/logHelper');
var router = require('./router');

var app = express();
var config = require('./config');

// ## export
module.exports = app;

// ## config

// ## routes
app.use('/', router.router);

// ## processes
process.on('uncaughtException', function(err, another){
	log.error("uncaughtException - err = %s | another = %s", 
		JSON.stringify(err), JSON.stringify(another));
});
process.on('unhandledRejection', (reason, p) => {
	log.error('Unhandled Rejection at:', p, 'reason:', reason);
});
process.on('exit', function(code){
	log.info('exit - code = %s', code);
});

// start server
app.listen(config.PORT, function(){
	log.info("Server started - port = %s", config.PORT);
});

// tls check and create
tls.createCert(registry.services);


